#ifndef ASTAR_HPP_
#define ASTAR_HPP_


#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <sstream>
#include <mutex>
class Point
{
public:
	int x;
	int y;
	int z;
	Point(int an_x, int an_y, int a_z):x(an_x),y(an_y), z(a_z)
	{

	}
};

class Size
{
public:
	int width;
	int height;
	int x;
	int y;
	int z;
};

struct Vertex
{
		/**
		 *
		 */
		Vertex( int anX,
				int anY,
				int aZ) :
						x( anX),
						y( anY),
						z( aZ),
						actualCost( 0.0),
						heuristicCost( 0.0)
		{
		}
		/**
		 *
		 */
		Vertex( const Point& aPoint) :
						x( aPoint.x),
						y( aPoint.y),
						z( aPoint.z),
						actualCost( 0.0),
						heuristicCost( 0.0)
		{
		}
		/**
		 *
		 */
		Point asPoint() const
		{
			return Point( x, y, z);
		}
		/**
		 *
		 */
		bool lessCost( const Vertex& aVertex) const
		{
			if (heuristicCost < aVertex.heuristicCost)
				return true;
			// less uncertainty if the actual cost is smaller
			if (heuristicCost == aVertex.heuristicCost)
				return actualCost > aVertex.actualCost;
			return false;
		}
		/**
		 *
		 */
		bool lessId( const Vertex& aVertex) const
		{
			if(x < aVertex.x) return true;
			if(x == aVertex.x) return y < aVertex.y;
			if(x == aVertex.x && y == aVertex.y) return z < aVertex.z;
					return false;
		}
		/**
		 *
		 */
		bool equalPoint( const Vertex& aVertex) const
		{
			return x == aVertex.x && y == aVertex.y && z == aVertex.z;
		}
		bool operator==(const Vertex& aVertex) const
		{
			return x == aVertex.x && y == aVertex.y && z == aVertex.z;
		}
		int x;
		int y;
		int z;

		double actualCost;
		double heuristicCost;
};
// struct Vertex

/**
 *
 */
struct VertexLessCostCompare : public std::binary_function< Vertex, Vertex, bool >
{
		bool operator()(	const Vertex& lhs,
							const Vertex& rhs) const
		{
			return lhs.lessCost( rhs);
		}
};
// struct VertexCostCompare
/**
 *
 */
struct VertexLessIdCompare : public std::binary_function< Vertex, Vertex, bool >
{
		bool operator()(	const Vertex& lhs,
							const Vertex& rhs) const
		{
			return lhs.lessId( rhs);
		}
};
// struct VertexIdCompare
/**
 *
 */
struct VertexEqualPointCompare : public std::binary_function< Vertex, Vertex, bool >
{
		bool operator()(	const Vertex& lhs,
							const Vertex& rhs) const
		{
			return lhs.equalPoint( rhs);
		}
};
// struct VertexPointCompare

struct Edge
{
		Edge(	const Vertex& aVertex1,
				const Vertex& aVertex2,
				const bool is_enabled = true):
						vertex1( aVertex1),
						vertex2( aVertex2),
						enabled(is_enabled)
		{
		}
		Edge( const Edge& anEdge) :
						vertex1( anEdge.vertex1),
						vertex2( anEdge.vertex2),
						enabled(anEdge.enabled)
		{
		}

		bool operator==(const Edge& anEdge) const
		{
			return vertex1 == anEdge.vertex1 && vertex2 == anEdge.vertex2;
		}
		bool isConnectedTo( const Vertex& aVertex) const
		{
			if(vertex1 == aVertex) return true;
			if(vertex2 == aVertex) return true;
			return false;
		}
		const Vertex& thisSide( const Vertex& aVertex) const
		{
			if (vertex1.equalPoint( aVertex))
				return vertex1;
			if (vertex2.equalPoint( aVertex))
				return vertex2;
			throw std::logic_error( "thisSide: huh???");
		}

		const Vertex& otherSide( const Vertex& aVertex) const
		{
			if (vertex1.equalPoint( aVertex))
				return vertex2;
			if (vertex2.equalPoint( aVertex))
				return vertex1;
			throw std::logic_error( "otherSide: huh???");
		}

		Vertex vertex1;
		Vertex vertex2;
		bool enabled;
};
// struct Edge

inline std::ostream& operator<<( 	std::ostream& os,
									const Vertex & aVertex)
{
	return os << "(" << aVertex.x << "," << aVertex.y << "," << aVertex.z<< "), " << aVertex.actualCost << " " << aVertex.heuristicCost;
}

inline std::ostream& operator<<( 	std::ostream& os,
									const Edge& anEdge)
{
	return os << anEdge.vertex1 << " -> " << anEdge.vertex2;
}

typedef std::vector< Vertex > OpenSet;
typedef std::set< Vertex, VertexLessIdCompare > ClosedSet;
typedef std::map< Vertex, Vertex, VertexLessIdCompare > VertexMap;



struct Graph
{
	std::vector<Vertex> vertex_list;
	std::vector<Edge> edge_list;
};


class AStar
{
	public:
	const std::vector<Edge> get_enabled_edges(const std::vector<Edge>& all_edges);
	std::vector< Vertex > GetNeighbours(	const Vertex& aVertex);
	std::vector< Edge > GetNeighbourConnections(	const Vertex& aVertex);
		/**
		 *
		 */
		std::vector< Vertex > search(	const Point& aStartPoint,
										const Point& aGoalPoint);
		/**
		 *
		 */
		std::vector< Vertex > search( 	Vertex aStart,
										const Vertex& aGoal);
		/**
		 *
		 */
		void addToOpenSet( const Vertex& aVertex);
		/**
		 *
		 */
		void removeFromOpenSet( const Vertex& aVertex);
		/**
		 *
		 */
		void removeFromOpenSet( OpenSet::iterator& i);
		/**
		 *
		 */
		OpenSet::iterator findInOpenSet( const Vertex& aVertex);
		/**
		 *
		 */
		bool findRemoveInOpenSet( const Vertex& aVertex);
		/**
		 *
		 */
		void removeFirstFromOpenSet();
		/**
		 *
		 */
		void addToClosedSet( const Vertex& aVertex);
		/**
		 *
		 */
		void removeFromClosedSet( const Vertex& aVertex);
		/**
		 *
		 */
		void removeFromClosedSet( ClosedSet::iterator& i);
		/**
		 *
		 */
		ClosedSet::iterator findInClosedSet( const Vertex& aVertex);
		/**
		 *
		 */
		bool findRemoveClosedSet( const Vertex& aVertex);
		/**
		 *
		 */
		ClosedSet getClosedSet() const;
		/**
		 *
		 */
		OpenSet getOpenSet() const;
		/**
		 *
		 */
		VertexMap getPredecessorMap() const;

		Graph mygraph;
		std::vector<Edge> forbidden_edges;

	protected:
		/**
		 *
		 */
		ClosedSet& getCS();
		/**
		 *
		 */
		const ClosedSet& getCS() const;
		/**
		 *
		 */
		OpenSet& getOS();
		/**
		 *
		 */
		const OpenSet& getOS() const;
		/**
		 *
		 */
		VertexMap& getPM();
		/**
		 *
		 */
		const VertexMap& getPM() const;

	private:
		/**
		 *
		 */
		ClosedSet closedSet;
		/**
		 *
		 */
		OpenSet openSet;
		/**
		 *
		 */
		VertexMap predecessorMap;
		/**
		 *
		 */

		mutable std::recursive_mutex openSetMutex;
		mutable std::recursive_mutex closedSetMutex;
		mutable std::recursive_mutex predecessorMapMutex;

};
// class AStar

#endif // ASTAR_HPP_
