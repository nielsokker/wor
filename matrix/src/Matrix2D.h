/*
 * Matrix2D.h
 *
 *  Created on: 22 sep. 2016
 *      Author: Niels Okker
 */

#ifndef MATRIX2D_H_
#define MATRIX2D_H_

#include <array>
#include <cstdint>
#include <iterator>
#include <initializer_list>
#include <iosfwd>
#include <vector>
#include <iostream>
#include <stdlib.h>


template<typename T, std::size_t rows, std::size_t columns>
class Matrix {
public:
	static_assert(rows > 0, "Amount of rows must be bigger than 0");
	static_assert(columns > 0, "Amount of columns must be bigger than 0");

	Matrix();
	Matrix(std::initializer_list<T> data);
	Matrix(const Matrix& a_matrix);
	virtual ~Matrix();

	T& at(std::size_t x, std::size_t y);
	T get(std::size_t x, std::size_t y) const;

	Matrix<T,columns,rows>get_transposed() const;

	Matrix<T,rows,columns>& operator=(const Matrix& a_matrix) ;
	bool operator==(const Matrix& a_matrix) const;

	void set_max_offset(T a_single_offset, T a_total_offset);

	template<typename U, std::size_t rows2, std::size_t columns2>
	Matrix<T,rows,columns2> operator*(const Matrix<U,rows2,columns2>& a_matrix) const;
	Matrix<T,rows,columns> operator*(const T a_scalar) const;

	Matrix<T,rows,columns> operator+(const Matrix& a_matrix) const;
	Matrix<T,rows,columns> operator+(const T a_scalar) const;

	Matrix<T,rows,columns> operator-(const Matrix& a_matrix) const;
	Matrix<T,rows,columns> operator-(const T a_scalar) const;



	template<typename U, std::size_t a_rows, std::size_t a_columns>
	friend std::ostream&  operator<<(std::ostream& stream, const Matrix<U,a_rows,a_columns>& matrix) ;


private:
	std::array<std::array<T, columns>, rows> the_matrix;
	T max_single_offset;
    T max_total_offset;
};

#include "Matrix2D.inl"

#endif /* MATRIX2D_H_ */
