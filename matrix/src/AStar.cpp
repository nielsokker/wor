#include "AStar.hpp"
#include <iostream>
#include <iterator>
//#include "Shape2DUtils.hpp"

/**
 *
 */
double ActualCost(	const Vertex& aStart,
					const Vertex& aGoal)
{
	return std::sqrt( (aStart.x - aGoal.x) * (aStart.x - aGoal.x) +
					  (aStart.y - aGoal.y) * (aStart.y - aGoal.y) +
					  (aStart.z - aGoal.z) * (aStart.z - aGoal.z));
}
/**
 *
 */
double HeuristicCost(	const Vertex& aStart,
						const Vertex& aGoal)
{
	return std::sqrt( (aStart.x - aGoal.x) * (aStart.x - aGoal.x) +
					  (aStart.y - aGoal.y) * (aStart.y - aGoal.y) +
					  (aStart.z - aGoal.z) * (aStart.z - aGoal.z));
}
/**
 *
 */
std::vector< Vertex > ConstructPath( 	VertexMap& aPredecessorMap,
										const Vertex& aCurrentNode)
{
	VertexMap::iterator i = aPredecessorMap.find( aCurrentNode);
	if (i != aPredecessorMap.end())
	{
		std::vector< Vertex > path = ConstructPath( aPredecessorMap, (*i).second);
		path.push_back( aCurrentNode);
		return path;
	} else
	{
		std::vector< Vertex > path;
		path.push_back( aCurrentNode);
		return path;
	}
}
/**
 *
 */
std::vector< Vertex > AStar::GetNeighbours(	const Vertex& aVertex)
{
	std::vector< Vertex > neighbours;
	for(auto a: mygraph.edge_list)
	{
		if(a.vertex1.equalPoint(aVertex) || a.vertex2.equalPoint(aVertex))
		{
			neighbours.push_back(a.otherSide(aVertex));
		}
	}
	return neighbours;
}
/**
 *
 */
std::vector< Edge > AStar::GetNeighbourConnections(	const Vertex& aVertex
												)
{
/*	std::vector< Edge > connections;

	const std::vector< Vertex >& neighbours = GetNeighbours( aVertex);
	for (const Vertex& vertex : neighbours)
	{
		connections.push_back( Edge( aVertex, vertex));
	}

	return connections;*/
	std::vector<Edge> connections;
	for(const Edge& edge : mygraph.edge_list)
	{
		if(edge.isConnectedTo(aVertex) && edge.enabled)
		{
			connections.push_back(edge);
		}
	}
	return connections;
}
/**
 *
 */
std::vector< Vertex > AStar::search(	const Point& aStartPoint,
										const Point& aGoalPoint)
{
	Vertex start( aStartPoint);
	Vertex goal( aGoalPoint);

	std::vector< Vertex > path = AStar::search( start, goal);
	return path;
}
/**
 *
 */
std::vector< Vertex > AStar::search( 	Vertex aStart,
										const Vertex& aGoal)
{
	getOS().clear();
	getCS().clear();
	getPM().clear();

	//int radius = std::sqrt( (aRobotSize.x / 2.0) * (aRobotSize.x / 2.0) + (aRobotSize.y / 2.0) * (aRobotSize.y / 2.0));
	//std::cerr << "aRobotSize = (" << aRobotSize.x << "," << aRobotSize.y << "), radius = " << radius << std::endl;

	long long begin = std::clock();

	aStart.actualCost = 0.0; 													// Cost from aStart along best known path.
	aStart.heuristicCost = aStart.actualCost + HeuristicCost( aStart, aGoal);	// Estimated total cost from aStart to aGoal through y.

	addToOpenSet(aStart);

	while (!openSet.empty())
	{
		Vertex current = *openSet.begin();

		if (current.equalPoint( aGoal))
		{
			std::cout << "Duration: " << (std::clock() - begin) << " openSet: " << getOS().size() << " closedSet: " << getCS().size() << " predecessorMap: " << getPM().size() << std::endl;

			return ConstructPath( predecessorMap, current);
		} else
		{
			addToClosedSet( current);
			removeFirstFromOpenSet();

			const std::vector< Edge >& connections = GetNeighbourConnections( current);

			for (const Edge& connection : connections)
			{
				//Vertex neighbour = (*std::find(vertices.begin(),vertices.end(),connection.otherSide(current)));
				Vertex neighbour = connection.otherSide( current);

				// The new costs
				double calculatedActualNeighbourCost = current.actualCost + ActualCost( current, neighbour);
				double totalHeuristicCostNeighbour = calculatedActualNeighbourCost + HeuristicCost( neighbour, aGoal);

				OpenSet::iterator openVertex = findInOpenSet( neighbour);
				if (openVertex != openSet.end())
				{
					// if neighbour is in the openSet we may have found a shorter via-route
					if ((*openVertex).heuristicCost <= totalHeuristicCostNeighbour)
					{
						continue;
					} else
					{
						removeFromOpenSet( openVertex);
					}
				}
				ClosedSet::iterator closedVertex = findInClosedSet( neighbour);
				if (closedVertex != closedSet.end())
				{
					// if neighbour is in the closedSet we may have found a shorter via-route
					if ((*closedVertex).heuristicCost <= totalHeuristicCostNeighbour)
					{
						continue;
					} else
					{
						removeFromClosedSet( closedVertex);
					}
				}

				neighbour.actualCost = calculatedActualNeighbourCost;
				neighbour.heuristicCost = totalHeuristicCostNeighbour;

				std::pair< VertexMap::iterator, bool > insertResult1 = predecessorMap.insert( std::make_pair( neighbour, current));
				if (insertResult1.second != true)
				{
					if (!(*insertResult1.first).first.equalPoint( neighbour))
					{
						std::cerr << "*** (*insertResult1.first).first != neighbour:\n\t\t" << (*insertResult1.first).first << " <- " << (*insertResult1.first).second << "\n\t\t" << neighbour
										<< " <- " << current << std::endl;
					}

					if ((*insertResult1.first).first.heuristicCost > neighbour.heuristicCost)
					{
						predecessorMap.erase( insertResult1.first);
						std::pair< VertexMap::iterator, bool > insertResult2 = predecessorMap.insert( std::make_pair( neighbour, current));
						if (insertResult2.second != true)
						{
							std::cerr << "**** Failed updating neighbour in the predecessorMap:  " << neighbour << " <- " << current << std::endl;
							if (insertResult2.first != predecessorMap.end())
							{
								std::cerr << "\tcurrent value in the predecessorMap:  " << (*insertResult2.first).first << " <- " << (*insertResult2.first).second << std::endl;
							} else
							{
								std::cerr << "\tinvalid iterator " << std::endl;
							}
						} else
						{
						}
					}
				} else
				{
				}

				// if neighbour is not in openSet, add it to the openSet
				// which should always be the case?
				openVertex = findInOpenSet( neighbour);
				if (openVertex == openSet.end())
				{
					addToOpenSet( neighbour);
				} else
				{
					std::cerr << "**** Failed adding neighbour to the openSet:  " << neighbour << std::endl;
				}
			}
			std::iter_swap(openSet.begin(), std::min_element( openSet.begin(), openSet.end(), VertexLessCostCompare()));
		}
	}
	std::cout << "Duration: " << (std::clock() - begin) << " openSet: " << getOS().size() << " closedSet: " << getCS().size() << " predecessorMap: " << getPM().size() << std::endl;

	std::cerr << "**** No route from " << aStart << " to " << aGoal << std::endl;
	return std::vector< Vertex >();
}
/**
 *
 */
void AStar::addToOpenSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	openSet.push_back( aVertex);
}
/**
 *
 */
void AStar::removeFromOpenSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	OpenSet::iterator i = findInOpenSet( aVertex);
	removeFromOpenSet( i);
}
/**
 *
 */
void AStar::removeFromOpenSet( OpenSet::iterator& i)
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	openSet.erase( i);
}
/**
 *
 */
OpenSet::iterator AStar::findInOpenSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	return std::find_if( openSet.begin(),
	                     openSet.end(),
	                     [aVertex](const Vertex& rhs)
	                     {
							return aVertex.equalPoint(rhs);
	                     });
}
/**
 *
 */
bool AStar::findRemoveInOpenSet( const Vertex& aVertex)
{
	std::lock_guard< std::recursive_mutex > lock( openSetMutex);
	OpenSet::iterator i = findInOpenSet( aVertex);
	if (i != openSet.end())
	{
		openSet.erase( i);
		return true;
	}
	return false;

}
/**
 *
 */
void AStar::removeFirstFromOpenSet()
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	openSet.erase( openSet.begin());
}
/**
 *
 */
void AStar::addToClosedSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	closedSet.insert( aVertex);
}
/**
 *
 */
void AStar::removeFromClosedSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	ClosedSet::iterator i = findInClosedSet( aVertex);
	removeFromClosedSet( i);
}
/**
 *
 */
void AStar::removeFromClosedSet( ClosedSet::iterator& i)
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	closedSet.erase( i);
}
/**
 *
 */
ClosedSet::iterator AStar::findInClosedSet( const Vertex& aVertex)
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	//return std::find_if(closedSet.begin(),closedSet.end(),[aVertex](const Vertex& rhs){return aVertex.equalPoint(rhs);});
	return closedSet.find( aVertex);
}
/**
 *
 */
ClosedSet AStar::getClosedSet() const
{
	std::lock_guard< std::recursive_mutex > lock( closedSetMutex);
	ClosedSet c = getCS();
	return c;
}
/**
 *
 */
bool AStar::findRemoveClosedSet( const Vertex& aVertex)
{
	std::lock_guard< std::recursive_mutex > lock( closedSetMutex);
	ClosedSet::iterator i = findInClosedSet( aVertex);
	if (i != closedSet.end())
	{
		closedSet.erase( i);
		return true;
	}
	return false;
}
/**
 *
 */
OpenSet AStar::getOpenSet() const
{
	std::lock_guard< std::recursive_mutex > lock( openSetMutex);
	OpenSet o = getOS();
	return o;
}
/**
 *
 */
VertexMap AStar::getPredecessorMap() const
{
	std::lock_guard< std::recursive_mutex > lock( predecessorMapMutex);
	VertexMap p = getPM();
	return p;
}

/**
 *
 */
ClosedSet& AStar::getCS()
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	return closedSet;
}
/**
 *
 */
const ClosedSet& AStar::getCS() const
{
	std::unique_lock< std::recursive_mutex > lock( closedSetMutex);
	return closedSet;
}
/**
 *
 */
OpenSet& AStar::getOS()
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	return openSet;
}
/**
 *
 */
const OpenSet& AStar::getOS() const
{
	std::unique_lock< std::recursive_mutex > lock( openSetMutex);
	return openSet;
}
/**
 *
 */
VertexMap& AStar::getPM()
{
	std::unique_lock< std::recursive_mutex > lock( predecessorMapMutex);
	return predecessorMap;
}
/**
 *
 */
const VertexMap& AStar::getPM() const
{
	std::unique_lock< std::recursive_mutex > lock( predecessorMapMutex);
	return predecessorMap;
}

/*
const std::vector<Edge> AStar::get_enabled_edges(const std::vector<Edge>& all_edges)
{
	std::vector<Edge> enabled_edges;

	return all_edges;
}
*/
