/*
 * main.cpp
 *
 *  Created on: 22 sep. 2016
 *      Author: Judith
 */
#include "Matrix2D.h"
#include <iostream>
#include "inversekinematics.h"
#include "AStar.hpp"
int main()
{
	try
	{
		//full test
		inverse_kinematics ik;

		Matrix<double,2,1> goal {10,20};
		ik.get_angles_belonging_to_new_position(goal);


		//test for moore penrose
/*
		Matrix<float,2,3> orig {1, 2, 3,
								6, 5, 4};
		Matrix<float,3,2> inv = ik.get_moore_penrose_inverse(orig);
		std::cout << inv <<std::endl;
		std::cout << "eenheids is " <<std::endl;
		std::cout << inv * orig << std::endl;
*/

		//test for 3x3
/*	Matrix<double,3,3> orig {37, 32,  27,
							 32, 29, 26,
							 27, 26,  25};
		std::cout << orig << std::endl;
		std::cout << ik.get_inverse_of_3x3(orig) << std::endl;
		std::cout << orig * ik.get_inverse_of_3x3(orig) << std::endl;*/


/*		Point start_point(0,0,1);
		Point end_point(10,10,3);
		AStar a;
		a.mygraph.vertex_list.push_back(Vertex( 0,0,1));
		a.mygraph.vertex_list.push_back(Vertex( 1,3,1));
		a.mygraph.vertex_list.push_back(Vertex( 3,2,1));
		a.mygraph.vertex_list.push_back(Vertex( 3,0,1));
		a.mygraph.vertex_list.push_back(Vertex( 1,9,1));
		a.mygraph.vertex_list.push_back(Vertex( 4,5,1));
		a.mygraph.vertex_list.push_back(Vertex( 6,3,1));
		a.mygraph.vertex_list.push_back(Vertex( 6,1,1));
		a.mygraph.vertex_list.push_back(Vertex( 2,8,1));
		a.mygraph.vertex_list.push_back(Vertex( 5,7,1));
		a.mygraph.vertex_list.push_back(Vertex( 7,5,1));
		a.mygraph.vertex_list.push_back(Vertex( 8,1,1));
		a.mygraph.vertex_list.push_back(Vertex( 4,8,1));
		a.mygraph.vertex_list.push_back(Vertex( 8,9,1));
		a.mygraph.vertex_list.push_back(Vertex( 9,4,1));
		a.mygraph.vertex_list.push_back(Vertex( 6,9,1));
		a.mygraph.vertex_list.push_back(Vertex( 4,10,1));
		a.mygraph.vertex_list.push_back(Vertex( 10,7,1));
		a.mygraph.vertex_list.push_back(Vertex( 9,6,1));
		a.mygraph.vertex_list.push_back(Vertex( 10,10,2));

		a.mygraph.edge_list.push_back(Edge( Vertex( 0,0,1), Vertex( 1,3,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 0,0,1), Vertex( 3,2,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 0,0,1), Vertex( 3,0,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 1,3,1), Vertex( 1,9,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 1,3,1), Vertex( 2,8,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 3,2,1), Vertex( 4,5,1), true));
		a.mygraph.edge_list.push_back(Edge( Vertex( 3,2,1), Vertex( 6,3,1), true));
		a.mygraph.edge_list.push_back(Edge( Vertex( 3,2,1), Vertex( 6,1,1), true));
		a.mygraph.edge_list.push_back(Edge( Vertex( 3,0,1), Vertex( 6,1,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 1,9,1), Vertex( 2,8,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 4,5,1), Vertex( 4,8,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 4,5,1), Vertex( 5,7,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 4,5,1), Vertex( 7,5,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 6,3,1), Vertex( 7,5,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 6,3,1), Vertex( 9,4,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 6,3,1), Vertex( 8,1,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 6,1,1), Vertex( 8,1,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 2,8,1), Vertex( 4,8,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 5,7,1), Vertex( 8,9,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 7,5,1), Vertex( 10,7,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 8,1,1), Vertex( 9,4,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 4,8,1), Vertex( 6,9,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 4,8,1), Vertex( 4,10,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 9,4,1), Vertex( 10,7,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 9,4,1), Vertex( 9,6,1)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 6,9,1), Vertex( 10,10,2)));
		a.mygraph.edge_list.push_back(Edge( Vertex( 10,7,1), Vertex( 10,10,2)));

		std::vector< Vertex > list = a.search(start_point,end_point);

		for(auto a: list)
		{
			std::cout << a << std::endl;
		}*/

	}
	catch (const char* msg) {
     std::cout << msg << std::endl;
    }
	catch (std::exception &e)
	{
			std::cerr<<"Caught exception: "<<e.what()<<"\n";
	}
	catch(...)
	{
		std::cerr << "something went wrong" << std::endl;
	}
   return 0;
}
