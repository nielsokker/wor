template<typename T, std::size_t rows, std::size_t columns>
void Matrix<T,rows,columns>::set_max_offset(T a_single_offset, T a_total_offset)
{
	max_single_offset = a_single_offset;
	max_total_offset = a_total_offset;

}
//                              default constructor
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns>::Matrix():max_single_offset(0),max_total_offset(0) {
	for(std::size_t x= 0; x < rows; x++)
	{
	 the_matrix.at(x).fill(0);
	}
}


//                              member initlist constructor
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns>::Matrix(std::initializer_list<T> data):max_single_offset(0),max_total_offset(0){
	std::vector<T> data_v = data;
	for(std::size_t x= 0; x < rows; x++)
	{
		for(std::size_t y= 0; y < columns; y++)
		{
			the_matrix.at(x).at(y) = data_v.at((columns * x) + y);
		}
	}
}

//                             copy constructor
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns>::Matrix(const Matrix& a_matrix):max_single_offset(a_matrix.max_single_offset)
		,max_total_offset(a_matrix.max_total_offset){
 the_matrix = a_matrix.the_matrix;
}

//                          default destructor
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns>::~Matrix() {

}

//                    at function
template<typename T, std::size_t rows, std::size_t columns>
T& Matrix<T,rows,columns>::at(std::size_t x, std::size_t y)
{

 return the_matrix.at(x).at(y);
}

                           // == operator
template<typename T, std::size_t rows, std::size_t columns>
bool  Matrix<T,rows,columns>::operator==(const Matrix& a_matrix) const
{
	T biggest_single_offset = 0;
    T total_offset = 0;

	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			 T current_offset = ((the_matrix.at(x).at(y) > a_matrix.get(x,y)) ? (the_matrix.at(x).at(y) - a_matrix.get(x,y)):(a_matrix.get(x,y) - the_matrix.at(x).at(y))  );
			 total_offset += current_offset;
			 if(current_offset > biggest_single_offset)
			 {
				 biggest_single_offset = current_offset;
			 }
		 }
	 }
	 //std::cout << "single biggest: " << biggest_single_offset << ", total offset: " << total_offset << std::endl;
	 return biggest_single_offset <= max_single_offset && total_offset <= max_total_offset;
}

//                   get function
template<typename T, std::size_t rows, std::size_t columns>
T Matrix<T,rows,columns>::get(std::size_t x, std::size_t y) const
{
 return the_matrix.at(x).at(y);
}

//                 transpose function
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,columns,rows> Matrix<T,rows,columns>::get_transposed() const
{
	Matrix<T,columns,rows> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			temp.at(y,x) = the_matrix.at(x).at(y);

		 }
	 }
	return temp;
}

//                        operator * with matrix
template<typename T, std::size_t rows, std::size_t columns>
template<typename U, std::size_t rows2, std::size_t columns2>
Matrix<T,rows,columns2> Matrix<T,rows,columns>::operator*(const Matrix<U,rows2,columns2>& a_matrix) const
{
	if(rows2 != columns)
	{
		throw "nColumns of first matrix must be equal to nRows of second matrix";
	}
	Matrix<T,rows,columns2> temp;
	for(std::size_t a_row = 0; a_row < rows; a_row++)
	{
		for(std::size_t a_col2 = 0; a_col2 < columns2; a_col2++)
		{
			T temp_number =0;
			for(std::size_t a_col =0; a_col < columns; a_col++)
			{
				temp_number += the_matrix.at(a_row).at(a_col) * a_matrix.get(a_col,a_col2);
			}
			temp.at(a_row,a_col2) = temp_number;
		}
	}
	return temp;
}
//                        operator * with scalar
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns> Matrix<T,rows,columns>::operator*(const T a_scalar) const
{

	Matrix<T,rows,columns> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			temp.at(x,y) = the_matrix.at(x).at(y) * a_scalar;

		 }
	 }
	return temp;
}
//                    operator + with scalar
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns> Matrix<T,rows,columns>::operator+(const T a_scalar) const
{
	Matrix<T,rows,columns> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			temp.at(x,y) = the_matrix.at(x).at(y) + a_scalar;

		 }
	 }
	return temp;
}

//                    operator + with matrix
template<typename T, std::size_t rows, std::size_t columns>
Matrix <T,rows,columns> Matrix<T,rows,columns>::operator+(const Matrix& a_matrix) const
{
	Matrix<T,rows,columns> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {

			temp.at(x,y) = the_matrix.at(x).at(y) +a_matrix.get(x,y);

		 }
	 }
	return temp;
}


//                    operator - with scalar
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns> Matrix<T,rows,columns>::operator-(const T a_scalar) const
{
	Matrix<T,rows,columns> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			temp.at(x,y) = the_matrix.at(x).at(y) - a_scalar;

		 }
	 }
	return temp;
}

//                    operator - with matrix
template<typename T, std::size_t rows, std::size_t columns>
Matrix <T,rows,columns> Matrix<T,rows,columns>::operator-(const Matrix& a_matrix) const
{
	Matrix<T,rows,columns> temp;
	 for(std::size_t x= 0; x < rows; x++)
	 {
		 for(std::size_t y= 0; y < columns; y++)
		 {
			 temp.at(x,y) = the_matrix.at(x).at(y) -a_matrix.get(x,y);
		 }
	 }
	return temp;
}

//                     assignment operator
template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns>& Matrix<T,rows,columns>::operator=(const Matrix& a_matrix)
{
	if(this != &a_matrix )
	{
		the_matrix = a_matrix.the_matrix;
	}
	return *this;
}
//                       operator <<
template<typename T, std::size_t rows, std::size_t columns>
std::ostream& operator <<(std::ostream& stream, const Matrix<T,rows,columns>& matrix){
 for(std::size_t i= 0; i < rows; i++)
 {
	 for(std::size_t j= 0; j < columns; j++)
	 {
		stream <<  matrix.get(i,j);
		if(j != columns -1)
		{
			 stream << " ";
		}
	 }
	 stream << std::endl;
 }
 return stream;
}

//                       matrix + scalar
template<typename U, std::size_t a_rows, std::size_t a_columns>
Matrix<U,a_rows,a_columns> operator+(const Matrix<U,a_rows,a_columns>& a_matrix, const U scalar)
{
 return a_matrix.operator+(scalar);
}

//						scalar + matrix
template<typename U, std::size_t a_rows, std::size_t a_columns>
Matrix<U,a_rows,a_columns> operator+(const U scalar, const Matrix<U,a_rows,a_columns>& a_matrix)
{
 return a_matrix.operator+(scalar);
}