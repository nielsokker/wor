/*
 * inversekinematics.h
 *
 *  Created on: Sep 26, 2016
 *      Author: niels
 */

#ifndef INVERSEKINEMATICS_H_
#define INVERSEKINEMATICS_H_
#include<iostream>
#include "Matrix2D.h"
#include <math.h>

#define N_ANGLES 3

class inverse_kinematics {
public:
	inverse_kinematics():current_angles({20,40,15}),length_of_arm({12,10,3}),beta(0.1)
	{
		current_endpoint = get_position_from_angles(current_angles);
	}


	virtual ~inverse_kinematics()
	{

	}

	template<typename T, std::size_t rows, std::size_t columns>
	Matrix<T,rows,columns> get_inverse_of_3x3(const Matrix<T,rows,columns>& a_matrix);

	template<typename T, std::size_t rows, std::size_t columns>
	Matrix<T,3,2> get_moore_penrose_inverse(const Matrix<T,rows,columns>& a_matrix);

	template<typename T>
	Matrix<T,3,1> get_angles_belonging_to_new_position(Matrix<T,2,1> goal);

	template<typename T>
    Matrix<T,2,1> get_position_from_angles(const Matrix<T,3,1>& angles);

	template<typename T>
	Matrix<T,2,3> calculate_jacobian(const Matrix<T,3,1>& angles);

	bool check_if_one_of_angles_is_incorrect();
private:
	template<typename T>
	T to_rad(T a_number)
	{
		return a_number * M_PI/180;
	}

	Matrix<double,2,1>current_endpoint;
	Matrix<double,N_ANGLES,1> current_angles;
	Matrix<double,1,3> length_of_arm;
	double beta;
};

#include "inversekinematics.inl"


#endif /* INVERSEKINEMATICS_H_ */
