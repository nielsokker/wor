template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,rows,columns> inverse_kinematics::get_inverse_of_3x3(const Matrix<T,rows,columns>& a_matrix)
{
	T detA = a_matrix.get(0,0) * a_matrix.get(1,1) * a_matrix.get(2,2) +
			 a_matrix.get(0,1) * a_matrix.get(1,2) * a_matrix.get(2,0) +
			 a_matrix.get(0,2) * a_matrix.get(1,0) * a_matrix.get(2,1) -
			 a_matrix.get(0,2) * a_matrix.get(1,1) * a_matrix.get(2,0) -
			 a_matrix.get(0,1) * a_matrix.get(1,0) * a_matrix.get(2,2) -
			 a_matrix.get(0,0) * a_matrix.get(1,2) * a_matrix.get(2,1);
	std::cout << detA << std::endl;
	if(detA == 0)
	{
		throw "no inverse";
	}
	Matrix<T,3,3> to_return;
	to_return.at(0,0) =   a_matrix.get(1,1) * a_matrix.get(2,2) - a_matrix.get(1,2) * a_matrix.get(2,1);//A = ei -fh
	to_return.at(1,0) = -(a_matrix.get(1,0) * a_matrix.get(2,2) - a_matrix.get(1,2) * a_matrix.get(2,0));//B = -(di - fg)
	to_return.at(2,0) =   a_matrix.get(1,0) * a_matrix.get(2,1) - a_matrix.get(1,1) * a_matrix.get(2,0);//C = dh - eg
	 // second row1
	to_return.at(0,1) = -(a_matrix.get(0,1) * a_matrix.get(2,2) - a_matrix.get(0,2) * a_matrix.get(2,1));//D = -(bi -ch)
	to_return.at(1,1) =   a_matrix.get(0,0) * a_matrix.get(2,2) - a_matrix.get(0,2) * a_matrix.get(2,0);//E = (ai - cg)
	to_return.at(2,1) = -(a_matrix.get(0,0) * a_matrix.get(2,1) - a_matrix.get(0,1) * a_matrix.get(2,0));//F = -(ah - bg)
	// third row
	to_return.at(0,2) =   a_matrix.get(0,1) * a_matrix.get(1,2) - a_matrix.get(0,2) * a_matrix.get(1,1);//G = (bf -ce)
	to_return.at(1,2) = -(a_matrix.get(0,0) * a_matrix.get(1,2) - a_matrix.get(0,2) * a_matrix.get(1,0));//H = -(af-cd)
	to_return.at(2,2) =   a_matrix.get(0,0) * a_matrix.get(1,1) - a_matrix.get(0,1) * a_matrix.get(1,0); //I = (ae -bd)
	return to_return * (1/detA);
}


template<typename T, std::size_t rows, std::size_t columns>
Matrix<T,3,2> inverse_kinematics::get_moore_penrose_inverse(const Matrix<T,rows,columns>& a_matrix)
{
	auto j_tr =  a_matrix.get_transposed();
	//std:: cout << "j_tr" << std::endl;
	std:: cout << j_tr << std::endl;
	auto jtxj = j_tr * a_matrix;
	std::cout << "jtxj" << std::endl << jtxj << std::endl;
	auto inv_jtxj = get_inverse_of_3x3(jtxj);
	std::cout <<  "inverse of (transposed * original)" << std::endl << inv_jtxj << std::endl;
	std::cout << "3x3 * inv_3x3" << std::endl <<  jtxj * inv_jtxj << std::endl;
	auto j_inv = inv_jtxj * j_tr;
	std::cout << "j inv " << std::endl << j_inv << std::endl;
 return j_inv;
		 
		 
}


template<typename T>
Matrix<T,3,1> inverse_kinematics::get_angles_belonging_to_new_position(Matrix<T,2,1> goal)
{
	current_endpoint.set_max_offset(0.01,100);
	unsigned long counter = 0;
	while(!(current_endpoint == goal))
	{
		// compute pose
		Matrix<T,2,3> j = calculate_jacobian(current_angles);
		
		std::cout << counter << std::endl;
		std::cout << current_angles << std::endl;
		
		if(check_if_one_of_angles_is_incorrect())
		{
			Matrix<T,3,1> correction {0.1,0.1,0.1};
			current_angles = current_angles  + correction; 
		}
		//calculate inverse of jacobian
		//Matrix<T,3,2> inv_j = get_moore_penrose_inverse(j);
		Matrix<T,3,2> inv_j =j.get_transposed();
		//calculate delta e
		Matrix<T,2,1> delta_e = (goal - current_endpoint) * beta;
		
		//verschil in hoeken = inverse jacobian * verschil in x,y
		Matrix<T,3,1> delta_f = inv_j * delta_e;
		
		//nieuwe hoeken = oude hoeken + verschil
		current_angles = current_angles  + delta_f; 
	
		//nieuwe eindpunt = de positie van de nieuwe hoeken
		current_endpoint = get_position_from_angles(current_angles);
		 
		counter++;
		
	}
	std::cout << "!GOT IT!" << std::endl;
	std::cout << "endpoint is: " << std::endl << current_endpoint ;
	std::cout << "angle have become: " << std::endl << current_angles << std::endl;
	std::cout << "it took n while loops: " << counter << std::endl;
	return current_angles;
}

template<typename T>
Matrix<T,2,1> inverse_kinematics::get_position_from_angles(const Matrix<T,3,1>& angles)
{
	Matrix<T,2,1> current_endpoint;
	current_endpoint.at(0,0) = length_of_arm.get(0,0) * sin(to_rad(angles.get(0,0))) + 
			    			   length_of_arm.get(0,1) * sin(to_rad(angles.get(0,0) + angles.get(1,0))) + 
			                   length_of_arm.get(0,2) * sin(to_rad(angles.get(0,0) + angles.get(1,0) + angles.get(2,0)));
	
	current_endpoint.at(1,0) = length_of_arm.get(0,0) * cos(to_rad(angles.get(0,0))) + 
			                   length_of_arm.get(0,1) * cos(to_rad(angles.get(0,0) + angles.get(1,0))) + 
			                   length_of_arm.get(0,2) * cos(to_rad(angles.get(0,0) + angles.get(1,0) + angles.get(2,0)));
	return current_endpoint;
}

template<typename T>
Matrix<T,2,3> inverse_kinematics::calculate_jacobian(const Matrix<T,3,1>& angles)
{
	Matrix<T,2,3> jacobian;
	T all_three_angles = angles.get(0,0) + angles.get(1,0) + angles.get(2,0);
	T first_two_angles = angles.get(0,0) + angles.get(1,0);
	// for x as first row
	jacobian.at(0,2)= length_of_arm.get(0,2) * cos(to_rad(all_three_angles));
	jacobian.at(0,1)= length_of_arm.get(0,1) * cos(to_rad(first_two_angles)) + jacobian.at(0,2);
	jacobian.at(0,0)= length_of_arm.get(0,0) * cos(to_rad(angles.get(0,0))) +  jacobian.at(0,1);
	// for y, second row
	jacobian.at(1,2)= length_of_arm.get(0,2) * -sin(to_rad(all_three_angles));
	jacobian.at(1,1)= length_of_arm.get(0,1) * -sin(to_rad(first_two_angles)) + jacobian.at(1,2);
	jacobian.at(1,0)= length_of_arm.get(0,0) * -sin(to_rad(angles.get(0,0))) +  jacobian.at(1,1);
	return jacobian;
}

bool inverse_kinematics::check_if_one_of_angles_is_incorrect()
{
	for(unsigned short i =0; i < 3; i++)
	{
		if(current_angles.get(i,0) <= 0.001 && current_angles.get(i,0) > - 0.001)
		{
			//std::cout << "found wrong angle" << std::endl;
			//std::cout << current_angles << std::endl;
			return true;
		}
	}
	return false;
}
